

import { useState } from 'react'
import {Card, Button}  from 'react-bootstrap'
import PropTypes from 'prop-types'

export default function CourseCard({courseProp}) {

    const {name, description, price} = courseProp;


    const [count, setCount] = useState(0);
    const [seats, setSeat] = useState(30);
    console.log(useState(0))

    function enroll(){

        if (seats > 0) {
            setCount(count + 1);
            setSeat(seats - 1);
        } else {
            alert("no more seats.")
        }
        console.log('Enrollees: ' + count)
        console.log("Seats: "+ seats);
    }


    return(
            <Card id='courseComponent1' className='cardHighlight p-3'>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Text>{count} Enrolees </Card.Text>
                    <Button variant="primary" onClick={enroll}>Enroll</Button>
                </Card.Body> 
            </Card>
    )}



//Check if the CourseCard component us getting the correct prop type
CourseCard.propTypes ={
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired

    })
}


