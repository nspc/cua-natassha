
import './App.css';
import AppNavbar from './components/AppNavbar'
import Container from 'react-bootstrap/Container'

import Home from './pages/Home'
import Courses from './pages/Courses'


function App() {

  return (
    // React Fragments <></>
    <>
      <AppNavbar />
      <Container>
        <Home />
        <Courses/>
      </Container>

    </> 
  );
}

export default App;
