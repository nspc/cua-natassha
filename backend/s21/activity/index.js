/*
    1. Create a function named getUserInfo which is able to return an object. 

        The object returned should have the following properties:
        
        - key - data type

        - name - String
        - age -  Number
        - address - String
        - isMarried - Boolean
        - petName - String

        Note: Property names given is required and should not be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.

*/

function getUserInfo() {

  return {

    name: "Natassha Cua",
    age: 38,
    address: "53 C Benitez Street Quezon City",
    isMarried: true,
    petName: "Yogi"
  }
}

let userInfo = getUserInfo();
console.log("getUserInfo()")
console.log(userInfo);

/*
    2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
        
        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.


        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
    
*/

function getArtistsArray() {
  return ["Apink", "Super Junior", "Mamamoo", "IU", "Taeyeon"];
}

let artistsArray = getArtistsArray();
console.log("getArtistsArray()")
console.log(artistsArray);




/*
    3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/


function getSongsArray() {
  return ["The Wave", "Sorry Sorry", "Decalcomanie", "Blueming", "Why"];
}

let songsArray = getSongsArray();
console.log("getSongsArray()")
console.log(songsArray)


/*
    4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

        - Note: the array returned should have at least 5 elements as strings.
                function name given is required and cannot be changed.

        To check, create a variable to save the value returned by the function.
        Then log the variable in the console.

        Note: This is optional.
*/



function getMoviesArray() {
  return ["One More Chance", "The How's of Us", "She's Dating A Gangster", "My Amnesia Girl", "ÜnOfficially Yours"];
}

let moviesArray = getMoviesArray();
console.log("getMoviesArray()")
console.log(moviesArray)

/*
    5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

            - Note: the array returned should have numbers only.
                    function name given is required and cannot be changed.

            To check, create a variable to save the value returned by the function.
            Then log the variable in the console.

            Note: This is optional.
            
*/


function getPrimeNumberArray() {
  return [2,3,5,7,11];
}

let primeNumbersArray = getPrimeNumberArray();
console.log("getPrimeNumberArray()")
console.log(primeNumbersArray)

// Solution in JS Quiz
let q1 = 8/2 * (2+2);
console.log(q1); //16

let x = 1387;
let y = 7831;

let quotient = x/y;
console.log(quotient);//0.17...

let q3 = 4*5 
console.log(q3)//20

let q4 = 3**4
//3 raised to the power 4
console.log(q4); //81

let q5 = 15%4;
console.log(q5);//3

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
        getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
        getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
        getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
        getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

    }
} catch(err){


}