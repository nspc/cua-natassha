console.log("Hi, B297!");

// Mini- Activity - log your favorite movie line 20 times in the console 
//Send a screenshot of your console.

function printline(){
	console.log("'Carpe diem. Seize the day, boys. Make your lives extraordinary.' - Dead Poets Society, 1989");
}

printline()

//Functions
//Lines/blocks of code that tell our device to perfrom a certain task when called/invoked.


//Function Declaratopn
/* 

Synatax:
function functionName(){
	code block (statement)
}


*/

//function declaration
function printName() {
	console.log("My name is Sha.");
}


//function invocation

printName();

declaredFunction();

//function declarations vs expressions
//function declaration is created with the function keyword and adding a function name
// they are "saved for later use"

function declaredFunction(){
	console.log("hello from declaredFunction");
}

declaredFunction();

//function expressions 
// is stored in a variable
// is an anonymous functions assigned to the variable function

//Cannot access before initialization 

let variableFunction = function(){
	console.log("Hello from the functiomn expression")
}

variableFunction()

// a function expression of a function named funcName assigned to the variable funcExpression

let funcExpression = function funcName(){
	console.log("hola");
}

funcExpression()

//We can also reassign declared functions and function expressions to new anonymous function

declaredFunction = function(){
	console.log("updated declaredFunction");
}

declaredFunction()

funcExpression= function(){
	console.log("updated funcExpression")
}

funcExpression()

// 

const constantFunc = function(){
	console.log("initialize with const")
}

constantFunc()

/*

constantFunc = function(){
	console.log("cannot be reassigned")
}

constantFunc();

*/

//functions scoping 

/*
scope - accessibility/visibility of variables
JS Variables has 3 types of scope:
1. local / block scope 
2. global scope
3. function scope


*/

//local or block scope

/*
{
let a = 1;
}
*/
//global scope
/*
let a = 1;
*/
//function scope
/*
function sample(){
	let a = 1;
}
*/

{
	let localVar = "Armando Perez";
	console.log(localVar);
}

let globalVar = "Mr. Worldwide";

//console.log(localVar); result in a error
console.log(globalVar);

function showName(){
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

console.log(functionVar);
console.log(functionConst);
console.log(functionLet);
}

showName()

//Nested functions

function myNewFuction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}

	nestedFunction()
}

myNewFuction()

//nestedFunction() is declared inside the myNewFunction Scope

//Function and global scoped variables

//global Scoped Variable

let globalName = "Cardo"

function myNewFunction2(){
	let nameInside = "Hillary"
	console.log(globalName)
}

myNewFunction2()
//console.loga(nameInside);

//using alert()

//alert("This will run immediately when the page loads")
/*

function showSampleAlert(){
	alert("Hello Earthlings! This is from a function")
}
showSampleAlert()

console.log("I will only log in the console when the alert is dismissed")

/*

//using prompt()

/*

let samplePrompt = prompt("Enter your Name: ");
console.log("Hi I am "+ samplePrompt);

*/

//prompt returns an empty string when there is no input or null if the user cancels the prompt()

/*
function printWelcomeMessage(){
	let firstName = prompt("Enter your first name: ")
	let lastName = prompt("Enter your last name: ")

	console.log("Hello, " + firstName + ' ' + lastName + "!")
	console.log("Welcome to my page")
}

printWelcomeMessage()

*/

// The return Statement 
// allow us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(){
	return "Jeffrey" + ' ' + "Smith" + ' '+"Bezos";
	console.log("This message will not be printed") 
}

//returnFullName()

let fullName = returnFullName();
console.log(fullName);

function returnFullAddress(){
	let fullAddress ={
		street: "#44 Maharlika St.", 
		city : "cainta", 
		province: "rizal"
	}

	return fullAddress
}

let myAddress = returnFullAddress()
console.log(myAddress)


function printPlayerInfo(){
	console.log("Username: "+ "dark_magician");
	console.log("level: "+ 95);
	console.log("Job: " +"Mage");
}

let user1 =printPlayerInfo()
console.log(user1) //undefined

function returnSumOf5and10(){
	//return 5+10;
}

let sumOf5And10 = returnSumOf5and10();
console.log(sumOf5And10)

let total= 100 + returnSumOf5and10();
console.log(total)

//NaN - not a number

function getGuildMembers(){
	return["Lulu","Tristana","Teemo"];
}

console.log(getGuildMembers())

//Functions naming conventions 

function getCourses(){
	let courses = ["ReactJS", "ExpressJS 101", "MongoDB 101"]
	return courses;
}

let courses = getCourses()
console.log(courses)

//Avoid using Generic Names and pointless and inappropriate function names
/*
function pikachu(){
	let color = "pink"
	return name;
}


*/

function displayCarInfo(){
	console.log("Brand: Toyota")
	console.log("Type: Sedan")
}
displayCarInfo()


