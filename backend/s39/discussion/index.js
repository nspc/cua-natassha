//JavaScript Synchronous vs Asynchronous
//JS by default is synchronous meaning that only one statemengt is executed at a time

/*
console.log("Hey!");
cosnole.log("Hola"); // error
console.log("i will run if no error is made")
*/

/*
console.log("Hi before the loop")

for(let i=0; i<=100; i++){
	console.log(i)
}

console.log("Hi after the loop")
*/

//Asynchronous means that we can proceed to execute other statements,  while time consuming code is running in the background

//[Getting All Posts]
//The Fetch API allows us to asynchronously request for a resources (data)

// A "promise" is an OBJECT that represents the eventual completion (or failure)  of an asynch function and its resulting value

//Promises is like a special container for a future value that may not be available immediately. The promise represents the outcomes of an asynch operation that may take some time to complete, such as fetching data from a server, reading a file, getting data from the database and any other task that doesn't happen instantly
//Syntax
	//fetch('url')

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

//the fetch() function returns a Promise which can then be chained using the .then() function. The then() function waits for the promise to be resolved before executing the code. 

//Syntax
	//fetch("url")
	//.then((response)=>{})

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response=> console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')//100
//Use the 'json' method from the "Response" object to CONVERT the data retrieved into JSON format to be used in our application
.then((response)=>response.json())
//log the converted JSON value from the "fetch" request
//when we have multiple .then methods, it created a promise chain
.then((posts)=>console.log(posts))

async function fetchData(){

	//waits for the "fetch" method to complete then stores the value in the "result" variable


	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	//result returned from fetch is a promise

	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	//convert the data from the response object as JSON
	let json = await result.json()
	console.log(json)
}


fetchData()

//[Retrieves a specific post]
fetch('https://jsonplaceholder.typicode.com/posts/18')
.then((response)=>response.json())
.then((json)=>console.log(json))

//[Creating a post]
fetch('https://jsonplaceholder.typicode.com/posts',{
	method: 'POST',
	headers:{
		'Content-type':'application/json'
	},
	body:JSON.stringify({
		title: 'New Post',
		body:'Hello World',
		userId:1
	})

})

.then((response)=>response.json())
.then((json)=>console.log(json));

//[Updating a post]
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers:{
		'Content-type':'application/json'
	},
	body:JSON.stringify({
		title: 'Corrected post',
	})

})

.then((response)=>response.json())
.then((json)=>console.log(json));

//[Deleting a post]
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'DELETE'

})

//[Filtering Posts]

//Syntax
	//Individual Parameters
		//'url?parameterName=value'
	//Multiple parameters
		//'url?paramA=valueA&paramB=valueB'

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response)=>response.json())
.then((json)=>console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=6')
.then((response)=>response.json())
.then((json)=>console.log(json))

//[Retrieve nested/related comments to post]
fetch('https://jsonplaceholder.typicode.com/post/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json));

