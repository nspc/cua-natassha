// Create a simple Express JS application

//Dependencies and modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Allows our backend application to be available to our frontend application
app.use(cors()); 


const userRoutes = require("./routes/user")
const courseRoutes = require("./routes/course")



//Database connection
	//Connect to our MongoDB Database
mongoose.connect("mongodb+srv://natasshapaulinecua:L2yKQcWBm1R2PwFk@cluster0.svmfj0s.mongodb.net/S43-S48?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);
	
	//prompt
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,'Connection error'));
	db.once('open', () => console.log('Connected to MongoDB Atlas.'));


//[Backend Routes]
//http://localhost:4000/users
	
app.use("/users",userRoutes);
app.use("/courses", courseRoutes);




//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}


module.exports = {app,mongoose};