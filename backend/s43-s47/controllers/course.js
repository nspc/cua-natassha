const Course = require("../models/Course");
const User = require("../models/User");

//Create a new course
/*
    Steps:
    1. Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    2. Uses the information from the request body to provide all the necessary information.
    3. Saves the created object to our database and add a successful validation true/false.
*/
module.exports.addCourse = (req, res) => {

    // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
    // Uses the information from the request body to provide all the necessary information
    let newCourse = new Course({
        name : req.body.name,
        description : req.body.description,
        price : req.body.price
    });

    // Saves the created object to our database
    return newCourse.save().then((course, error) => {

 
        if (error) {
            return res.send(false);

 
        } else {
            return res.send(true);
        }
    })
    .catch(err => res.send(err))
};

//Retrieve all courses
/*
    1. Retrieve all the courses from the database
*/

//We will use the find() method for our course model

module.exports.getAllCourses = (req,res)=>{
    return Course.find({}).then(result=>{
        return res.send(result)
    })
}

//getAllActiveCourses
//create a function that will handle req,res
//that will find all active courses in the db

module.exports.getAllActiveCourses = (req,res)=>{
    return Course.find({isActive:true}).then(result=>{
        return res.send(result)
    })
}
//Get a specific course
module.exports.getCourse = (req,res)=>{
    return Course.findById(req.params.courseId).then(result=>{
        return res.send(result)
    })
}

module.exports.updateCourse=(req,res)=>{

    let updateCourse = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
}
 return Course.findByIdAndUpdate(req.params.courseId, updateCourse).then((course,error)=>{
    if(error){
        return res.send(false);
    }else{
        return res.send(true);
    }
 })
}



//archive a specific couse
module.exports.archiveCourse = (req,res)=>{
    let archivedCourse = {
        isActive: false
    }

    return Course.findByIdAndUpdate(req.params.courseId, archivedCourse).then((course, error)=>{
        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    })
}


//activate a specific couse
module.exports.activateCourse = (req,res)=>{
    let activatedCourse = {
        isActive: true
    }

    return Course.findByIdAndUpdate(req.params.courseId, activatedCourse).then((course, error)=>{
        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }
    })
}
//Mini Activity - Contextualize the searchCourseByName controller
//  search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Controller action to get emails of enrolled users
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
    const courseId = req.params.courseId; // Use req.params to get route parameters
  
    try {
      // Find the course by courseId
      const course = await Course.findById(courseId);
  
      if (!course) {
        return res.status(404).json({ message: 'Course not found' });
      }
  
      // Get the userIds of enrolled users from the course
      const userIds = course.enrollees.map(enrollee => enrollee.userId);
  
      // Find the users with matching userIds
      const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Use userIds instead of undefined "users" variable
  
      // Extract the emails from the enrolled users
      const emails = enrolledUsers.map(user => user.email); // Use map instead of forEach
  
      res.status(200).json({ userEmails: emails }); // Correct the variable name userEmails
  
    } catch (error) {
      res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
    }
  };

// Controller to search courses based on price range
module.exports.searchCoursesByPriceRange = async (req, res) => {
    try {
      const { minPrice, maxPrice } = req.body;
  
      if (!minPrice || !maxPrice) {
        return res.status(400).json({ message: 'Both minPrice and maxPrice are required in the request body.' });
      }
  
      const courses = await Course.find({
        price: { $gte: minPrice, $lte: maxPrice },
      });
  
      return res.json({ courses: courses });
    } catch (error) {
      console.error(error);
      return res.status(500).json({ message: 'An error occurred while searching for courses.' });
    }
  };
  



