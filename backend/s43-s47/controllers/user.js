//controller
const User = require("../models/User");
const Course = require("../models/Course")

const bcrypt = require("bcrypt");
const auth = require("../auth")


//Check if the email exists already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}


	})
}

//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email:reqbody.email,
		mobileNo:reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true
		}

	})
	.catch(err=>err)
}

//User Authentication
/*
	Steps:
	1. check the db if user email exists
	2. compare the password from req.body and password stored in the db
	3. generate a JWT if the user logged in and returned false if not
*/

module.exports.loginUser = (req,res)=>{

return User.findOne({email: req.body.email}).then(result=>{
	if(result === null){
		return false;
	} else {

		//compareSync method is used to compare a non-encrypted password and from the encrypted password form the db
		const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

		if(isPasswordCorrect){
			return res.send({access: auth.createAccessToken(result)})
		}
		//else pw does not match
		else {
			return res.send(false);
		}
	}
})
.catch(err=>res.send(err))
}


//[ACTIVITY] Controller for retrieving user details 
/*
	Steps: 
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend.
*/

module.exports.getProfile = (req,res)=>{
	return User.findById(req.user.id).then(result =>{
		result.password = "";
		return res.send(result);
	})
	.catch(err => res.send(err))
}

//Enroll user to a class
/*
	Steps:
	1. Find the document in the db using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB atlast database

*/
module.exports.enroll = async(req,res)=>{

	//To check in the terminal the user id and the courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	//Check if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}
	//Create an "isUserUpdated" variable and return true upon successful update
	let isUserUpdated = await User.findById(req.user.id).then(user =>{

		//Add the courseId in an object and push that object inyo the user's "enrollment" array
		let newEnrollment ={
			courseId: req.body.courseId
		}

		//Add the course in the "enrollment" array
		user.enrollments.push(newEnrollment);

		//Save the updated user and return true if successful or the error message is failed
		return user.save().then(user => true).catch(err => err.message);
	});

	//Checks if there are error in updating the user. 
	if (isUserUpdated !== true){
		return res.send({message:isUserUpdated});
	}


	let isCourseUpdated = await Course.findById(req.body.courseId).then(course =>{

		let enrollee = {
			userId:req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message)
	})

	// checks if there are error in updating the course
	if(isCourseUpdated !== true){
		return res.send({message:isCourseUpdated});
	}

	//checks if both user update and course update are successful
	if (isUserUpdated && isCourseUpdated){
		return res.send({message:"Enrolled Successfully"})
	}
}

	//Getting user's enrolled courses
	module.exports.getEnrollments = (req, res)=>{
		return User.findById(req.user.id).then(result => 
			res.send(result.enrollments)).catch(err=> res.send(err))
			}
	
	// Function to reset the password
	module.exports.resetPassword = async (req, res) => {
  	
  	try {

  	//used object destructuring
    	const { newPassword } = req.body;

    // Extracting user ID from the authorization header
		const { id } = req.user;

    // Hashing the new password
    	const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    	await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    	res.status(200).json({ message: 'Password reset successfully' });

  	} catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  	}
};	

//Contextualize  Mini Activity
// Controller function to update the user profile
	module.exports.updateProfile = async (req, res)=> {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}

  // Controller to update user as an admin
module.exports.updateUserAsAdmin = async (req, res) => {
	try {
	  const { userId } = req.body;
  
	  if (!userId) {
		return res.status(400).json({ message: 'User ID is required in the request body.' });
	  }
  
	  const authenticatedUser = req.user; // Assuming authentication middleware is used
  
	  // Check if authenticated user is an admin
	  if (!authenticatedUser.isAdmin) {
		return res.status(403).json({ message: 'Access denied. Not an admin.' });
	  }
  
	  const userToUpdate = await User.findById(userId);
  
	  if (!userToUpdate) {
		return res.status(404).json({ message: 'User not found.' });
	  }
  
	  // Perform the admin update logic here, e.g. updating user's role to 'admin'
	  userToUpdate.isAdmin = true;
	  await userToUpdate.save();
  
	  return res.json({ message: 'User updated successfully.' });
	} catch (error) {
	  console.error(error);
	  return res.status(500).json({ message: 'An error occurred while updating the user.' });
	}
  };

// Controller to update enrollment status of a user for a specific course
module.exports.updateEnrollmentStatus = async (req, res) => {
	try {
	  const { userId, courseId, status } = req.body;
  
	  if (!userId || !courseId || !status) {
		return res.status(400).json({ message: 'userId, courseId, and status are required in the request body.' });
	  }
  
	  /* const authenticatedUser = req.user; // Assuming authentication middleware is used
  
	  // Check if authenticated user is an admin
	  if (!authenticatedUser.isAdmin) {
		return res.status(403).json({ message: 'Access denied. Not an admin.' });
	  } */
	  
  
	  const user = await User.findById(userId);
  
	  if (!user) {
		return res.status(404).json({ message: 'User not found.' });
	  }
  
	  const enrollment = user.enrollments.find(enrollment => enrollment.courseId === courseId);
  
	  if (!enrollment) {
		return res.status(404).json({ message: 'Enrollment not found.' });
	  }
  
	  enrollment.status = status;
	  await user.save();
  
	  return res.json({ message: 'Enrollment status updated successfully.' });
	} catch (error) {
	  console.error(error);
	  return res.status(500).json({ message: 'An error occurred while updating enrollment status.' });
	}
  };