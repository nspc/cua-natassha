
async function fruitsOnSale(db) {
	return await(

			db.fruits.aggregate([
			{$match:{onSale: true}}])

		);
};


async function fruitsInStock(db) {
	return await(

				db.fruits.aggregate([
				{$match: {stock: {$gte: 20}}},
				{$count: "Enough Stock"}
			])

		);
};


async function fruitsAvePrice(db) {
	return await(
			db.fruits.aggregate([
			{$match: { onSale: true}},
  			{$group: {_id: "$supplier_id",averagePrice: { $avg: "$price" }}},
  			{$sort: {price: -1}}
])

		);
};


async function fruitsHighPrice(db) {
	return await(

		db.fruits.aggregate([
        {$match:{onSale: true}},
        {$group:{_id: "$supplier_id",fruitsHighPrice: {$max: "$price"}}}
      ])

		);
};


async function fruitsLowPrice(db) {
	return await(

    	db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
			])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
