console.log("Intro to JSON!")

/*
	JSON
		JSON stands for JS Object Notation
		JSON is also used in other programming languages

		Syntax:

		{
			"propertyA":"valueA",
			"propertyA":"valueB"

		};

		JSON are wrapped in curly braces 
		Properties/Keys are wrapped in double quotations


*/

let sample1 = `
	{
		"name":"Jayson Derulo",
		"age": 20,
		"address": {
			"city":"Quezon City",
			"country": "Philippines"
		}
	}

`;

console.log(sample1);
console.log(typeof sample1);

//Are we able to turn a JSON into a JS object?
//YES.

console.log(JSON.parse(sample1));


//JSON Array 
	//JSON array is an array of JSON

	let sampleArr =`
	[
		{
			"email":"wackywizard@example.com",
			"password":"laughtOutLoud123",
			"isAdmin": true
		},

		{
			"email": "dalisay.cardo@gmail.com",
			"password": "alyanna4ever",
			"isAdmin":true
		},

		{
			"email":"lebroan4goat@ex.com",
			"password":"ginisamix",
			"isAdmin":false
		},

		{
			"email":"ilovejson@gmail.com",
			"password":"jsondontloveme",
			"isAdmin": false

		}

	]

	`

	//Can we use Array Methods on a JSON array?
	//NO . Because JSON is a string 
	console.log(typeof sampleArr)

	//So what can we do to be able to add more items/objects into our sampleArr?
	//PARSE the JSON array to a JS Array and save it in a variable

	let parsedSampleArr = JSON.parse(sampleArr)
	console.log(parsedSampleArr)
	console.log(typeof parsedSampleArr)

	//MA 
	//delete the last item in the JSON array
	console.log(parsedSampleArr.pop());

	//log in the console, parsedSampleArr
	console.log(parsedSampleArr);

	//if for example, we need to send this data back to our client/frontend, it should be on JSON format

	//JSON.parse() does not mutate or uodate the original JSON
		// we can actually turn a JS object into as JSON again

	//JSON.stringify() - this will stringify JS objects as JSON 

	sampleArr = JSON.stringify(parsedSampleArr);

	console.log(sampleArr)
	console.log(typeof sampleArr) //string

	//Databse (JSON) => Server/API (JSON to JS object to process) => Sent as JSON => Frontend/client

	let jsonArr =`

		[
			"pizza", 
			"hamburger", 
			"spaghetti",
			"shanghai",
			"hotdog stick on a pineapple",
			"pancit bihon"

		]

	`
	/*
		MA (5 mins)
		process it and convert it to a JS Object so we can manipulate the arry 
		Delete teh last item in the array 
		Add a new item in the array
		Stringify the array back to JSON
		add update jsonArr with the the stringified array


	*/
/*
	console.log(jsonArr)

	//Parse the JSON array to a JavaScript array
	let parsedArr = JSON.parse(jsonArr);
	console.log(parsedArr)

	//Delete the last item in the array
	console.log(parsedArr.pop());

	// Add a new item to the array
	console.log(parsedArr.push("sushi"));

	//Stringify the array back to JSON
	let updatedJsonArr = JSON.stringify(parsedArr);

	//Update jsonArr with the stringified array
	jsonArr = updatedJsonArr;
	console.log(jsonArr);


*/

	console.log(jsonArr)
	let parsedJsonArr = JSON.parse(jsonArr)
	parsedJsonArr.pop()
	parsedJsonArr.push("Sushi")
	jsonArr = JSON.stringify(parsedJsonArr)
	console.log(jsonArr)

//Gather User Details
/*
	let firstName = prompt("What is your first name?")
	let lastName = prompt("What is your last name?")
	let age = prompt("What is your age?")
	let address = {
		city: prompt("which city do you live in?"),
		country: prompt("which country does your city address belong to?")
	};

	let otherData = JSON.stringify({
		firstName:firstName,
		lastName:lastName,
		age:age,
		address:address
	})

console.log(otherData)
*/