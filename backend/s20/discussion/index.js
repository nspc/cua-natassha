console.log("Hello World");

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator:" + sum);

let difference = x - y;
console.log("Result of difference operator:" + difference);

let product = x * y;
console.log("Result of product operator:" + product);

let quotient = x / y;
console.log("Result of quotient operator:" + quotient);

let remainder = x % y;
console.log("Result of remainder operator:" + remainder);

let a = 10;
let b = 5;
let remainderA = a % b;
console.log("Capable of being divided: " + remainderA); //0 if capable of being divided.

// The modulo operator, denoted by %, is an arithmetic operator. The modulo division operator produces the remainder of an integer division which is also called the modulus of the operation.

//Addition Assignment Operator
let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2; //Re-add = 10
assignmentNumber += 2; //Re-assign = 12
console.log("Result of += : " + assignmentNumber);
//Subtraction Assignment Operator
assignmentNumber -= 2;   //Result = 10
console.log("Result of -= : " + assignmentNumber);
//Multiplication Assignment Operator
assignmentNumber *= 2;  //Result = 20
console.log("Result of *= : " + assignmentNumber);
//Division Assignment Operator
assignmentNumber /= 2;   //Result = 10
console.log("Result of /= : " + assignmentNumber);

// MDAS stands for Multiplication, Division, Addition, and Subtraction
/*
 M   3 * 4 = 12
 D   12 / 5 = 24
 A   1 + 2 = 3
 S   3 - 2.4 = 0.6
*/

let mdas = 1+2-3*4/5;
console.log("MDAS = " + mdas);

// PEMDAS is an acronym used to mention the order of operations to be followed while solving expressions having multiple operations. PEMDAS stands for P- Parentheses, E- Exponents, M- Multiplication, D- Division, A- Addition, and S- Subtraction.
/*
 P  4/5 = 0.8
 E  2-3 = -1
 M  -1 * 0.8 = 0.8
 D    - - -
 A   1 + -0.8 = 0.2
 S   
*/
let pemdas = 1 + (2-3) * (4/5);
console.log("PEMDAS = " + pemdas);

let z = 1;

// the value of z is added by a value of one before returning the value and stroing it in the variable "increment"

//increase the value first before assigning
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//the value of z is returned and stored in the variable "increment" then the value 

//re-assign and then increased
increment = z++;
console.log("Result of pre-increment: " + increment);
console.log("Result of post-increment: " + z);

//pre-decrement
//decrease then re-assign
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

//post-decrement
//re-assign and then decrease
decrement = z--;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coercion
/*
Type coercion is the 
*/
let numA = '10';
let numB = 12;
let coercion = numA+numB;
console.log("coercion: " + coercion); //1012

// declare and initialize
let NumC = 16;
let numD = 14;
let nonCoercion = NumC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//Conversion from Number to String, String to Number, Boolean to Number etc. when different types of operators are applied to the values

//boolean "true" is associated with the value of 1
let numE = true + 1;
console.log(numE);

//boolean "false" is associated with the value of 0
let numF = false + 1;
console.log(numF);


let juan = 'juan';

console.log("START HERE");
//Comparison Operators (==) true
console.log(1 == 1);//TRUE
console.log(1 == 2); //FALSE
console.log(1 == '1');//TRUE
console.log(0 == false);//TRUE
console.log('juan' == 'juan');//TRUE
console.log('juan' == juan);//TRUE

console.log("START HERE");
//Equality Operator (!=) false
console.log(1 != 1);//False
console.log(1 != 2); //TRUE
console.log(1 != '1');//False
console.log(0 != false);//False
console.log('juan' != 'juan');//False
console.log('juan' != juan);//False

// The strict equality ( === ) operator checks whether its two operands are equal, returning a Boolean result. Unlike the equality operator, the strict equality operator always considers operands of different (data) types to be different.

console.log("START HERE");
//Strict Equality Operator, ( === ) (data) types
console.log(1 === 1);//True
console.log(1 === 2);//False
console.log(1 === '1');//False
console.log(0 === false);//False
console.log('juan' === 'juan');//True
console.log('juan' === juan);//True

console.log("START HERE");
//Strict Equality Operator, (data) types (!=) false
console.log(1 !== 1);//False
console.log(1 !== 2);//True
console.log(1 !== '1');//True
console.log(0 !== false);//True
console.log('juan' !== 'juan');//False
console.log('juan' !== juan); //False

console.log("START HERE");
//Relational operator
let abc = 50;
let def = 65;
let isGreaterThan = abc > def;
let isLessThan = abc < def;
let isGTOrEqual = abc >= def;
let isLTOrEqual = abc <= def;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

console.log("START HERE");
let numStr ="30";
console.log(abc > numStr); //forced coercion
console.log(def <= numStr); 

console.log("START HERE");
let str = "twenty";
console.log(def >= str);

//Logical Operators
let isLegalAge = true;
let isRegistered = false;

//&& (and), ||(or), !(NOT)

//&& 
//Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of AND operator: " + allRequirementsMet); //false

//|| 
//Returns true of one operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of OR operator: " + someRequirementsMet); //true

//! 
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT operator: " + someRequirementsNotMet); //true
